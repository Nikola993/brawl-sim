#-------------------------------------------------
#
# Project created by QtCreator 2015-04-23T19:11:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BrawlSim
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    fighter.cpp \
    hpbar.cpp \
    fight.cpp \
    startmenu.cpp \
    endscene.cpp \
    hitbox.cpp \
    aboutdialog.cpp

HEADERS  += mainwindow.h \
    fighter.h \
    hpbar.h \
    fight.h \
    startmenu.h \
    endscene.h \
    hitbox.h \
    aboutdialog.h

FORMS    += mainwindow.ui \
    aboutdialog.ui

QMAKE_CXXFLAGS += -std=c++0x

RESOURCES += \
    resources.qrc
