#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::Dialog)
{
    ui->setupUi(this);

    ui->textEdit->setEnabled(false);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
