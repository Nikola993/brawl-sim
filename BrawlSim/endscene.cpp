#include "endscene.h"

EndScene::EndScene(int w,int h,QString victor)
{
    setSceneRect(QRect(0, -h, w, h));

    if(victor == QString("p1"))
        mVictorName = addText("Player 1 won!", QFont(QFont("Times", 30, QFont::Bold)));
    else
        mVictorName = addText("Player 2 won!", QFont(QFont("Times", 30, QFont::Bold)));

    mVictorName->setPos(200,-480);

    if(true)
    {
        mBackground = new QImage(QString(":sprites/slike/hram.jpg"));
    }
    else
    {
        mBackground = new QImage(QString(":sprites/slike/hram.jpg"));
    }
      QTimer::singleShot(2000, this, SLOT(end()));
}


void EndScene::drawBackground(QPainter *pPainter, const QRectF &rRect)
{
    QRectF sceneRect = this->sceneRect();

    pPainter->drawImage( sceneRect, *mBackground);

}

void EndScene::end()
{
    deleteLater();
}

EndScene::~EndScene()
{
    delete mBackground;

}

