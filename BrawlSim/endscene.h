#ifndef ENDSCENE_H
#define ENDSCENE_H
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QTimer>
#include <QKeyEvent>
#include <QPainter>

class EndScene : public QGraphicsScene
{
    Q_OBJECT
public:
   EndScene(int w, int h, QString);
    void drawBackground(QPainter* pPainter, const QRectF& rRect);
    ~EndScene();


public slots:
     void end();
private:
       QGraphicsTextItem *mVictorName;
       QImage *mBackground;
};

#endif // ENDSCENE_H
