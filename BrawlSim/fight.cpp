#include "fight.h"

Fight::Fight(int w, int h, QMenuBar *menu)
{
    setSceneRect(QRect(0, -h, w, h));
    mBackground = new QImage(QString(":sprites/slike/hram.jpg"));

    mBar1 = new HpBar(0, -480, QString("Player1"), 15, true);
    mBar2 = new HpBar(390, -480, QString("Player2"), 15, false);

    mPlayer1 = new Fighter(100, -170, false, "p1", mBar1);
    mPlayer2 = new Fighter(300,-170, true, "p2", mBar2);

    mPlayer2->setOpponent(mPlayer1);
    mPlayer1->setOpponent(mPlayer2);

    addItem(mPlayer1);
    addItem(mPlayer2);
    addItem(mBar1);
    addItem(mBar2);

    QAction *mShowBoxes = new QAction(QString("Show Hitboxes"), this);
    menu->addAction(mShowBoxes);
    mShowBoxes->setCheckable(true);
    connect(mShowBoxes, SIGNAL(toggled(bool)), mPlayer1, SLOT(drawHitboxes(bool)));
    connect(mShowBoxes, SIGNAL(toggled(bool)), mPlayer2, SLOT(drawHitboxes(bool)));

    connect(&mTimer, SIGNAL(timeout()),this, SLOT(advance()));
    connect(mPlayer1, SIGNAL(dead(QString)), this, SLOT(fightOver(QString)));
    connect(mPlayer2, SIGNAL(dead(QString)), this, SLOT(fightOver(QString)));
    mTimer.start(1000/60);
}


void Fight::fightOver(QString victor)
{
    emit victorName(victor);
    deleteLater();
}

void Fight::keyPressEvent(QKeyEvent* event)
{
    switch(event->key())
    {
        case Qt::Key_A:
            if(mPlayer1->state() != Fighter::State::ATTACKING && mPlayer1->state() != Fighter::State::ATTACKED &&
                    mPlayer1->state() != Fighter::State::MOVING_RIGHT)
            {
                mPlayer1->setXvel(-10);
                mPlayer1->setStateMovingLeft();
            }
            break;
        case Qt::Key_D:
            if(mPlayer1->state() != Fighter::State::ATTACKING && mPlayer1->state() != Fighter::State::ATTACKED &&
                    mPlayer1->state() != Fighter::State::MOVING_LEFT)
            {
                mPlayer1->setXvel(10);
                mPlayer1->setStateMovingRight();
            }
            break;
        case Qt::Key_X:
            if(event->isAutoRepeat())
                return;
            mPlayer1->setStateAttacking(Qt::Key_X);
            break;
      /*  case Qt::Key_C:
            if(event->isAutoRepeat())
                return;
            mPlayer1->setStateAttacking(Qt::Key_C);
            break;*/
        case Qt::Key_F:
            if(mPlayer2->state() != Fighter::State::ATTACKING && mPlayer2->state() != Fighter::State::ATTACKED &&
                    mPlayer2->state() != Fighter::State::MOVING_RIGHT)
            {
                mPlayer2->setXvel(-10);
                mPlayer2->setStateMovingLeft();
            }
            break;
        case Qt::Key_H:
            if(mPlayer2->state() != Fighter::State::ATTACKING && mPlayer2->state() != Fighter::State::ATTACKED &&
                    mPlayer2->state() != Fighter::State::MOVING_LEFT)
            {
                mPlayer2->setXvel(10);
                mPlayer2->setStateMovingRight();
            }
            break;
        case Qt::Key_B:
            if(event->isAutoRepeat())
                return;
            mPlayer2->setStateAttacking(Qt::Key_B);
            break;
        /*case Qt::Key_V:
            if(event->isAutoRepeat())
                return;
            mPlayer2->setStateAttacking(Qt::Key_V);
            break;*/
        case Qt::Key_P:
            {
            if(mPause == false)
            {
                mTimer.stop();
                mPause=true;
            }
            else if(mPause == true)
            {
                mTimer.start(1000/60);
                mPause=false;
            }
        }
       case Qt::Key_Z:
           if(mPlayer1->state() != Fighter::State::ATTACKING && mPlayer1->state() != Fighter::State::ATTACKED &&
                mPlayer1->state() != Fighter::State::MOVING_RIGHT  &&  mPlayer1->state() != Fighter::State::MOVING_LEFT)
             {
                     mPlayer1->setStateGuard();
                }
            break;
       case Qt::Key_N:
        if(mPlayer2->state() != Fighter::State::ATTACKING && mPlayer2->state() != Fighter::State::ATTACKED &&
                mPlayer2->state() != Fighter::State::MOVING_RIGHT &&  mPlayer2->state() != Fighter::State::MOVING_LEFT)
        {
            mPlayer2->setStateGuard();
        }
        break;
    }
}

void Fight::keyReleaseEvent(QKeyEvent *event)
{
    switch(event->key())
    {
        case Qt::Key_A:
            if(mPlayer1->state() == Fighter::State::MOVING_LEFT)
            {
                mPlayer1->setXvel(10);
                mPlayer1->setStatePassive();
            }
            break;
        case Qt::Key_D:
            if(mPlayer1->state() == Fighter::State::MOVING_RIGHT)
            {
                mPlayer1->setXvel(-10);
                mPlayer1->setStatePassive();
            }
            break;
        case Qt::Key_X:
            break;
        case Qt::Key_F:
            if(mPlayer2->state() == Fighter::State::MOVING_LEFT)
            {
                mPlayer2->setXvel(10);
                mPlayer2->setStatePassive();
            }
            break;
        case Qt::Key_H:
            if(mPlayer2->state() == Fighter::State::MOVING_RIGHT)
            {
                mPlayer2->setXvel(-10);
                mPlayer2->setStatePassive();
            }
            break;
        case Qt::Key_B:
            break;
        case Qt::Key_Z:
        if(mPlayer1->state() == Fighter::State::GUARD)
        {
            mPlayer1->setStatePassive();
        }
         break;
        case Qt::Key_N:
        if(mPlayer2->state() == Fighter::State::GUARD)
        {
            mPlayer2->setStatePassive();
        }
     break;

    }
}

void Fight::drawBackground(QPainter* pPainter, const QRectF& rRect)
{
    QRectF sceneRect = this->sceneRect();

    pPainter->drawImage( sceneRect, *mBackground);
}

Fight::~Fight()
{
    delete mBackground;
    delete mBar1;
    delete mBar2;
    mPlayer1->deleteLater();
    mPlayer2->deleteLater();
}
