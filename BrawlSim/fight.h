#ifndef FIGHT_H
#define FIGHT_H
#include <QGraphicsScene>
#include <QTimer>
#include "fighter.h"
#include "hpbar.h"
#include <QMenuBar>

class Fight : public QGraphicsScene
{
    Q_OBJECT

public:
    Fight(int w, int h, QMenuBar *menu);
    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);
    void drawBackground(QPainter* pPainter, const QRectF& rRect);
    virtual ~Fight();

signals:
    void victorName(QString);

public slots:
    void fightOver(QString);

private:
    QImage *mBackground;
    Fighter *mPlayer1, *mPlayer2;
    HpBar *mBar1, *mBar2;
    QTimer mTimer;
    bool mPause;
};

#endif // FIGHT_H
