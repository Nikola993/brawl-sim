#include "fighter.h"
#include <QDebug>

Fighter::Fighter(int x, int y, bool inv, QString name, HpBar *bar) :mXvel(0),mHealth(15),mName(name), mDrawBoxes(false)
{
    setPos(x,y);

    mBar = bar;

    mState = PASSIVE;
    mInverted = inv;

    mWrap = new QGraphicsRectItem(boundingRect(),this);
    mBody = new QGraphicsRectItem(5, 5, 70, 140,mWrap);

    box = new HitBox();
    box->setmActiveBlue(mBody);

    if(mInverted)
    {
        mNeu = new QPixmap(QString(":sprites/slike/Normalan2.png"));
        mAtt = new QPixmap(QString(":sprites/slike/Udarac2.png"));
        //postaviti sprite za guard
        mGrd = new QPixmap(QString(":sprites/slike/Gard2.png"));

        box->setmReds(Qt::Key_B, new QGraphicsRectItem(70,40,40,20,mWrap));
      //box->setmReds(Qt::Key_V, new QGraphicsRectItem(70,40,80,20,mWrap));
       //get method za mreds
        // mReds[Qt::Key_B]->setVisible(false);
        mWrap->setTransform(QTransform().translate(mWrap->boundingRect().width(),0).scale(-1,1),true);
    }
    else
    {

        mNeu = new QPixmap(QString(":sprites/slike/Normalan1.png"));
        mAtt = new QPixmap(QString(":sprites/slike/Udarac1.png"));
        //postaviti sprite za guard
        mGrd = new QPixmap(QString(":sprites/slike/Gard1.png"));
        box->setmReds(Qt::Key_X, new QGraphicsRectItem(70,40,40,20,mWrap));
        //box->setmReds(Qt::Key_C, new QGraphicsRectItem(70,40,80,20,mWrap));
        //getMethod za mReds
        //mReds[Qt::Key_X]->setVisible(false);
    }

    mPicture = new QGraphicsPixmapItem(*mNeu,this);

    mWrap->setVisible(false);
    mPicture->setVisible(false);
    mBody->setVisible(false);
    //inicijalizovati hitboxove
   // mBlues << new QGraphicsRectItem(10, 10, 60, 30, mBody)
   //     << new QGraphicsRectItem(10, 50, 60, 75, mBody);
    box->setmActiveRed();
}

QRectF Fighter::boundingRect() const
{
    return QRectF(0, 0, 150, 150);
}

QPainterPath Fighter::shape() const
{
    QPainterPath path;
    path.addRect(mBody->rect());
    return path;
}


void Fighter::setOpponent(Fighter* opp)
{
    mOpponent = opp;
}

void Fighter::setStateAttacking(Qt::Key key)
{
    if(mState != ATTACKING)
    {
        mState = ATTACKING;

        box->setmActiveRed(box->activemReds(key));
 //       if(key==Qt::Key_X || key==Qt::Key_B){
            QTimer::singleShot(500, this, SLOT(setStatePassive()));
            mXvel = 0;
 //           }
  /*      else{
            QTimer::singleShot(500, this, SLOT(setStateLongRecover()));
            mXvel = 0;
            }*/
    }
}

void Fighter::setStateAttacked()
{
    mState = ATTACKED;
    mXvel = 0;
}

void Fighter::setStatePassive()
{
    if(mState != PASSIVE)
    {
        mState = PASSIVE;
        box->setmActiveRed();
        box->setmActiveBlue(mBody);

        scene()->update(boundingRect());
    }
}

void Fighter::setStateMovingLeft()
{
    if(mState != MOVING_LEFT){
        mState = MOVING_LEFT;
    }

}

void Fighter::setStateMovingRight()
{
    if(mState != MOVING_RIGHT){
        mState = MOVING_RIGHT;
    }
}

Fighter::State Fighter::state()
{
    return mState;
}

void Fighter::setStateGuard()
{
    if(mState != GUARD){
        mState = GUARD;
        box->setmActiveBlue();
    //    QTimer::singleShot(500,this,SLOT(setStateBlockRecover()));
    }

}

void Fighter::setStateBlockRecover(){
    QTimer::singleShot(100,this,SLOT(setStatePassive()));
}

void Fighter::setStateLongRecover(){
    mState=ATTACKING;
    box->setmActiveRed();
    QTimer::singleShot(300,this,SLOT(setStatePassive()));
}

void Fighter::setStateRecover(){
    mState=ATTACKING;
    box->setmActiveRed();
    QTimer::singleShot(150,this,SLOT(setStatePassive()));
}

void Fighter::drawHitboxes(bool val)
{
    mDrawBoxes = val;
}

void Fighter::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    /* za pocetak, iscrtati bounding rect, rect za telo, kao i nasumicne bluebox-ove */

    QPen pen(Qt::black);

    painter->setPen(pen);

    painter->drawPixmap(boundingRect(),mPicture->pixmap(),mPicture->pixmap().rect());

    if(mInverted)
        painter->setTransform(QTransform().translate(boundingRect().width(),0).scale(-1,1),true);

    if(mDrawBoxes)
    {
       painter->drawRect(boundingRect());
       painter->drawRect(mBody->rect());

        /*for(const auto& r : mBlues)
            painter->drawRect(r->rect());*/
    }

    if(box->activeRed())
    {
        if(mDrawBoxes)
            painter->drawRect(box->activeRed()->rect());
        mPicture->setPixmap(*mAtt);
        scene()->update();
    }
    else if(mState == GUARD)
    {
        mPicture->setPixmap(*mGrd);
        scene()->update();
    }
    else
    {
        mPicture->setPixmap(*mNeu);
        scene()->update();
    }

}

void Fighter::setXvel(int val)
{

    mXvel += val;
     qDebug() << "Move!";
}


void Fighter::advance(int phase)
{
    if(!phase)
        return;

    setPos(mapToScene(mXvel,0));
    collisionCheck();
}




int Fighter::damage()
{
    mHealth-=1;
    return -1;
}

int Fighter::getHealth()
{
    return mHealth;
}

QString Fighter::getName()
{
    return mName;
}

void Fighter::collisionCheck()
{
    QRectF bodyRect = mapToScene(mBody->rect()).boundingRect();

    if(mInverted)
    {
        if(bodyRect.right() < scene()->sceneRect().left())
            setX(scene()->sceneRect().left() - mBody->rect().right());
        else if(bodyRect.right()+bodyRect.width() > scene()->sceneRect().right())
            setX(scene()->sceneRect().right() - mBody->rect().right()-bodyRect.width());
    }
    else
    {
        if(bodyRect.left() < scene()->sceneRect().left())
            setX(scene()->sceneRect().left() - mBody->rect().left());
        else if(bodyRect.right() > scene()->sceneRect().right())
            setX(scene()->sceneRect().right() - mBody->rect().right());
    }

    if(mBody->collidesWithItem(mOpponent->mBody))
    {
        setPos(mapToScene(-mXvel,0));
    }

    if(box->activeBlue() != nullptr && box->activeBlue()->collidesWithItem(mOpponent->box->activeRed()))
    {
        mBar->updateHp(damage());
                if(mInverted)
                {
                    setPos(mapToScene(60,0));
                    setStateRecover();
                    QTimer::singleShot(1000, this, SLOT(setStatePassive()));
                }
                else
                {
                    setPos(mapToScene(-60,0));
                    setStateRecover();
                    QTimer::singleShot(1000, this, SLOT(setStatePassive()));
                }
                if(!mHealth)
                    emit dead(mOpponent->getName());
                qDebug().nospace() << "Hit!" << this->getName() << this->getHealth() << mOpponent->getName() << mOpponent->getHealth();
    }
}

Fighter::~Fighter()
{
    delete mPicture;
    delete mNeu;
    delete mAtt;
    delete mGrd;

    for(auto& r : mBlues)
        delete r;

    if(mInverted)
        delete mReds[Qt::Key_B];
    else
        delete mReds[Qt::Key_X];

    delete mBody;
    delete mWrap;
}

