#ifndef FIGHTER_H
#define FIGHTER_H
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QKeyEvent>
#include <QPainter>
#include <QDebug>
#include <QTimer>
#include <QTransform>
#include "hpbar.h"
#include "hitbox.h"

class Fighter : public QObject, public QGraphicsItem
{
    Q_OBJECT

public:

    enum State
    {
        ATTACKING, ATTACKED, PASSIVE, MOVING_LEFT, MOVING_RIGHT, GUARD
    };

    Fighter(int x, int y, bool inv, QString name, HpBar *bar);
    void setXvel(int val);
    void setOpponent(Fighter* opp);
    void collisionCheck();
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void advance(int phase);
    State state();
    QGraphicsRectItem* activeRed();
    QGraphicsRectItem* activeBlue();
    ~Fighter();
    int damage();
    int getHealth();
    QString getName();


signals:
   void dead(QString);

public slots:
    void setStateAttacking(Qt::Key key);
    void setStateAttacked();
    void setStatePassive();
    void setStateMovingLeft();
    void setStateMovingRight();
    void setStateGuard();
    void setStateRecover();
    void setStateBlockRecover();
    void setStateLongRecover();
    void drawHitboxes(bool val); 

private:
    int mXvel;
    int mHealth;
    bool mInverted;
    bool mDrawBoxes;
    QGraphicsPixmapItem *mPicture;
    QPixmap *mNeu;
    QPixmap *mAtt;
    QPixmap *mGrd;
    QPixmap *m;
    State mState;
    Fighter *mOpponent;
    QGraphicsRectItem *mBody;
    QGraphicsRectItem *mWrap;
    QList<QGraphicsRectItem*> mBlues;
    QMap<Qt::Key,QGraphicsRectItem*> mReds;
    //QGraphicsRectItem *mActiveRed;
    //QGraphicsRectItem *mActiveBlue;
    QString mName;
    HpBar *mBar;
    HitBox *box;

};

#endif // FIGHTER_H
