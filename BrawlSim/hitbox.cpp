#include "hitbox.h"



HitBox::HitBox()
{
    mActiveRed=nullptr;
    mActiveBlue=nullptr;
}

void HitBox::setmActiveRed(QGraphicsRectItem *rect)
{
    mActiveRed=rect;
}

void HitBox::setmActiveBlue(QGraphicsRectItem *rect)
{
    mActiveBlue=rect;
}

QGraphicsRectItem* HitBox::activeRed()
{
    return mActiveRed;
}

QGraphicsRectItem* HitBox::activeBlue()
{
    return mActiveBlue;
}

QGraphicsRectItem *HitBox::activemReds(Qt::Key key)
{
    return mReds[key];
}

void HitBox::setmActiveRed()
{
    mActiveRed= nullptr;
}

void HitBox::setmActiveBlue()
{
    mActiveBlue=nullptr;
}

void HitBox::setmReds(Qt::Key key, QGraphicsRectItem* rect)
{
    mReds[key]= rect;
}

HitBox::~HitBox()
{
    delete mActiveBlue;
    delete mActiveRed;
}

