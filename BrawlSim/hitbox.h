#ifndef HITBOX_H
#define HITBOX_H
#include <QGraphicsRectItem>
#include <QKeyEvent>

class HitBox
{
public:
    HitBox();

    //dodati u listu mblue(graphicRectItem)44.l

    void setmActiveRed(QGraphicsRectItem*);
    void setmActiveBlue(QGraphicsRectItem*);
    QGraphicsRectItem* activeRed();
    QGraphicsRectItem* activeBlue();
    QGraphicsRectItem* activemReds(Qt::Key);

    void setmActiveRed();
    void setmActiveBlue();
    void setmReds(Qt::Key, QGraphicsRectItem*);


    ~HitBox();

private:
    QList<QGraphicsRectItem*> mBlues;
    QMap<Qt::Key,QGraphicsRectItem*> mReds;
    QGraphicsRectItem *mActiveRed;
    QGraphicsRectItem *mActiveBlue;
};

#endif // HITBOX_H
