#include "hpbar.h"

HpBar::HpBar(int x, int y, QString label, int maxHp, bool ltr)
    : mLabel(label), mMaxHp(maxHp), mLtr(ltr)
{
    setPos(x,y);
    mBackground = new QImage(QString(":sprites/slike/HeltBar.png"));
    mInterval = boundingRect().width() / mMaxHp;
    mCurrHp = mMaxHp;
    mBarRect = QRectF(boundingRect().topLeft(), QPointF(boundingRect().right(), boundingRect().top()+boundingRect().height()/2));
  //zeleni
    mLabelRect = QRectF(QPointF(boundingRect().left(), boundingRect().top()+boundingRect().height()/2), boundingRect().bottomRight());
}

QRectF HpBar::boundingRect() const
{
    return QRectF(25, 0, 200, 50);
}

void HpBar::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
   // painter->drawRect(boundingRect());
    painter->setBrush(QBrush(Qt::green));
    painter->setPen(QPen(Qt::black));

    if(mLtr)
    {
        QPointF topLeft = QPointF(boundingRect().right()-mInterval*mCurrHp,8);
        QPointF bottomRight = QPointF(boundingRect().right(),19);
        painter->drawImage(0,0,*mBackground);
        painter->drawRect(QRectF(topLeft,bottomRight));
       // painter->drawText(mLabelRect, Qt::AlignLeft, QString(mLabel));
    }
    else
    {
        QPointF topLeft = QPointF(boundingRect().left(),8);
        QPointF bottomRight = QPointF(boundingRect().left()+mInterval*mCurrHp,19);
        painter->drawImage(0,0,*mBackground);
        painter->drawRect(QRectF(topLeft,bottomRight));
       // painter->drawText(mLabelRect, Qt::AlignRight, QString(mLabel));
    }
}

void HpBar::updateHp(int val)
{
    mCurrHp+=val;
}


