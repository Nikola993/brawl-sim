#ifndef HPBAR_H
#define HPBAR_H
#include <QGraphicsItem>
#include <QPainter>
#include <QDebug>

class HpBar : public QGraphicsItem
{
public:
    HpBar(int x, int y, QString label, int maxHp, bool ltr);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void updateHp(int val);

private:
    QString mLabel;
    int mMaxHp;
    bool mLtr;
    int mInterval;
    int mCurrHp;
    QRectF mLabelRect;
    QRectF mBarRect;
    QImage *mBackground;
};



#endif // HPBAR_H
