#include "mainwindow.h"
#include "ui_mainwindow.h"

const int MainWindow::SCREEN_WIDTH = 640;
const int MainWindow::SCREEN_HEIGHT = 480;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->graphicsView->setSceneRect(QRect(0, -SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT));
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    setMenuScene();
    QAction *mAboutAction = new QAction("About",this);
    menuBar()->addAction(mAboutAction);
    connect(mAboutAction,SIGNAL(triggered()),this,SLOT(showAbout()));
}


MainWindow::~MainWindow()
{
    mFightScene->deleteLater();
    mStMenu->deleteLater();
    delete ui;
}

void MainWindow::setFightScene(int val)
{
    if(val == 1)
        qApp->exit();

    mFightScene = new Fight(SCREEN_WIDTH, SCREEN_HEIGHT, menuBar());
    ui->graphicsView->setScene(mFightScene);

    connect(mFightScene,SIGNAL(victorName(QString)),this, SLOT(setEndScene(QString)));
}


void MainWindow::setMenuScene()
{
    mStMenu = new StartMenu(SCREEN_WIDTH, SCREEN_HEIGHT);
    ui->graphicsView->setScene(mStMenu);
    connect(mStMenu,SIGNAL(makeSelection(int)),this, SLOT(setFightScene(int)));
}

void MainWindow::setEndScene(QString str)
{
    mEndScene = new EndScene(SCREEN_WIDTH, SCREEN_HEIGHT,str);

    ui->graphicsView->setScene(mEndScene);
    connect(mEndScene,SIGNAL(destroyed()),this, SLOT(setMenuScene()));
}

void MainWindow::showAbout()
{
    AboutDialog *diag = new AboutDialog;
    diag->setWindowTitle(QString("About"));
    diag->setModal(true);
    diag->show();
}

