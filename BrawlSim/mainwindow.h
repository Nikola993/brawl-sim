#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include "fight.h"
#include "startmenu.h"
#include "endscene.h"
#include "aboutdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    static const int SCREEN_WIDTH;
    static const int SCREEN_HEIGHT;
    ~MainWindow();

public slots:
    void setFightScene(int val);
    void setMenuScene();
    void setEndScene(QString);
    void showAbout();

private:
    Ui::MainWindow *ui;
    Fight *mFightScene;
    StartMenu *mStMenu;
    EndScene *mEndScene;

};

#endif // MAINWINDOW_H
