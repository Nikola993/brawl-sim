#include "startmenu.h"

StartMenu::StartMenu(int w, int h)
{
    setSceneRect(QRect(0, -h, w, h));
    mBackground = new QImage(QString(":sprites/slike/Background.png"));

    mTitle = addText(QString("Brawl sim"), QFont(QFont("Times", 20, QFont::Bold)));
    mStartSelection = addText(QString("Start game"), QFont(QFont("Times", 30, QFont::Bold)));
    mQuitSelection = addText(QString("Quit game"), QFont(QFont("Times", 30, QFont::Bold)));


    mTitle->setPos((w-mTitle->boundingRect().width())/2, -450);
    mStartSelection->setPos((w-mTitle->boundingRect().width())/2, -200);
    mQuitSelection->setPos((w-mTitle->boundingRect().width())/2, -100);
    mSelected = 0;

    changeSelected();
}


void StartMenu::changeSelected()
{
    if(mSelected == 0)
    {
        QFont f;
        f.setBold(true);
        mStartSelection->setFont(f);
        mQuitSelection->setFont(QFont());
    }
    else if(mSelected == 1)
    {
        QFont f;
        f.setBold(true);
        mStartSelection->setFont(QFont());
        mQuitSelection->setFont(f);
    }

}


void StartMenu::keyPressEvent(QKeyEvent* event)
{
    switch (event->key())
    {
        case Qt::Key_Up:
        case Qt::Key_W:
            mSelected = (mSelected-1)%2;
            changeSelected();
            break;
        case Qt::Key_Down:
        case Qt::Key_S:
            mSelected = (mSelected+1)%2;
            changeSelected();
            break;
        case Qt::Key_Enter:
        case Qt::Key_Return:
            deleteLater();
            emit makeSelection(mSelected);
            break;
        default:
            break;
    }

}

void StartMenu::keyReleaseEvent(QKeyEvent* event)
{

}

StartMenu::~StartMenu()
{
    delete mBackground;
    delete mTitle;
    delete mStartSelection;
    delete mQuitSelection;
}
void StartMenu::drawBackground(QPainter* pPainter, const QRectF& rRect)
{
    QRectF sceneRect = this->sceneRect();
    pPainter->drawImage( sceneRect, *mBackground);
}
