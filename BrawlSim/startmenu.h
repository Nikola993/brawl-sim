#ifndef STARTMENU_H
#define STARTMENU_H
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QTimer>
#include <QKeyEvent>
#include <QPainter>

class StartMenu : public QGraphicsScene
{
    Q_OBJECT
public:
    StartMenu(int w, int h);
    void changeSelected();
    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);
    void drawBackground(QPainter* pPainter, const QRectF& rRect);
    virtual ~StartMenu();

signals:
    void makeSelection(int );

private:
    QImage *mBackground;
    QGraphicsTextItem *mTitle;
    QGraphicsTextItem *mStartSelection;
    QGraphicsTextItem *mQuitSelection;
    int mSelected;
};

#endif // STARTMENU_H
